import { MAX_CRITICAL_HIT, MIN_CRITICAL_HIT } from '../../constants/hitPower';
import { FighterDetailsInterface } from '../interface/fighter-details.interface';

export async function fight(
  firstFighter: FighterDetailsInterface,
  secondFighter: FighterDetailsInterface): Promise<FighterDetailsInterface> {

  return new Promise((resolve): void => {
    document.addEventListener('keydown', keyDownEvent, false);
    document.addEventListener('keyup', keyUpEvent, false);

// TODO fight actions!!!
    function keyDownEvent(event: KeyboardEvent): void {
    }

    function keyUpEvent(event: KeyboardEvent): void {
      const isWinner = {};
      if (isWinner) {
        document.removeEventListener('keydown', keyDownEvent, false);
        document.removeEventListener('keyup', keyUpEvent, false);
        // TODO check winner function!!!
        resolve(firstFighter || secondFighter);
      }
    }

  });
}

export function getDamage(attacker: FighterDetailsInterface, defender: FighterDetailsInterface): number {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(0, damage);
}

export function getHitPower(fighter: FighterDetailsInterface): number {
  // return hit power
  return fighter.attack * (Math.random() * (MAX_CRITICAL_HIT - MIN_CRITICAL_HIT) + MIN_CRITICAL_HIT);
}

export function getBlockPower(fighter: FighterDetailsInterface): number {
  // return block power
  return fighter.defense * (Math.random() * (MAX_CRITICAL_HIT - MIN_CRITICAL_HIT) + MIN_CRITICAL_HIT);
}

export function getCriticalHitPower(fighter: FighterDetailsInterface): number {
  return fighter.attack * 2;
}

// TODO health indicator function

// export function updateHealthIndicator(position, fighter, fighterHealth) {
//   const bar = document.getElementById(`${position}-fighter-indicator`);
//   const healthFighter = (fighterHealth / fighter.health) * 100;
//   bar.style.width = `${healthFighter}%`;
//   if (healthFighter <= 20) {
//     bar.style.backgroundColor = 'red';
//   }
//   if (healthFighter <= 0) {
//     bar.style.width = '0';
//   }
// }

