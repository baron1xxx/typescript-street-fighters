import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { FighterDetailsInterface } from '../../interface/fighter-details.interface';


export function showWinnerModal(fighter: FighterDetailsInterface) {

  // call showModal function
  let title: string = `And the winner is - ${fighter.name}!`;
  let bodyElement: HTMLElement = createElement({ tagName: 'div', className: 'winner__body' });
  showModal({ title, bodyElement });
}
