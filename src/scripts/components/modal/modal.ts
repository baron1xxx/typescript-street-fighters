import { createElement } from '../../helpers/domHelper';
import App from '../../app';
import { ModalInterface } from '../../interface/modal.interface';

export function showModal({
                            title, bodyElement, onClose = (): void => {}
                          }: ModalInterface): void {
  const root = getModalContainer();
  const modal = createModal({ title, bodyElement, onClose });

  root.append(modal);
}

function getModalContainer(): HTMLElement {
  return <HTMLElement>document.getElementById('root');
}

function createModal({ title, bodyElement, onClose }: ModalInterface) {
  const layer = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
  const header = createHeader(title);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string): HTMLElement {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement = createElement({ tagName: 'span' });
  const closeButton = createElement({ tagName: 'div', className: 'close-btn' });

  titleElement.innerText = title;
  closeButton.innerText = '×';

  const close = (): void => {
    hideModal();
    document.getElementsByClassName('arena___root')[0].remove();
    new App();
  };
  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);

  return headerElement;
}

function hideModal(): void {
  const modal = <HTMLElement>document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
