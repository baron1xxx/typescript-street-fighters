import { createElement } from '../helpers/domHelper';
import { createFightersSelector } from './fighterSelector';
import { FighterDetailsInterface } from '../interface/fighter-details.interface';

export function createFighters(fighters: Array<FighterDetailsInterface>): HTMLElement {
  const selectFighter = createFightersSelector();
  const container = createElement({ tagName: 'div', className: 'fighters___root' });
  const preview = createElement({ tagName: 'div', className: 'preview-container___root' });
  const fightersList = createElement({ tagName: 'div', className: 'fighters___list' });
  const fighterElements = fighters.map((fighter) => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

function createFighter(
  fighter: FighterDetailsInterface,
  selectFighter: (event: Event, fighterId: string) => Promise<void>): HTMLElement {
  const fighterElement = createElement({ tagName: 'div', className: 'fighters___fighter' });
  const imageElement = createImage(fighter);
  const onClick = (event: Event): Promise<void> => selectFighter(event, fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}

function createImage(fighter: FighterDetailsInterface): HTMLElement {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  return createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes
  });
}
