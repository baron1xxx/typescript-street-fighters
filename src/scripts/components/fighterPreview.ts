import { createElement } from '../helpers/domHelper';
import { FighterDetailsInterface } from '../interface/fighter-details.interface';

export function createFighterPreview(fighter: FighterDetailsInterface, position: string): HTMLElement {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });
  if (fighter) {
    const fighterImage = createFighterImage(fighter);
    const fighterInfo = createFighterInfo(fighter);
    fighterElement.append(fighterImage, fighterInfo);
  }

  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter: FighterDetailsInterface): HTMLElement {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  return createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });
}

export function createFighterInfo(fighter: FighterDetailsInterface): HTMLElement {
  const fighterInfo = createElement({
    tagName: 'div',
    className: 'fighter-preview___info'
  });

  const elementsInfo = createElementsInfo(fighter);
  fighterInfo.append(...elementsInfo);

  return fighterInfo;
}

export function createElementsInfo(fighter: FighterDetailsInterface): Array<HTMLElement> {
  return Object.keys(fighter)
    .filter((element) => element !== '_id' && element !== 'source')
    .map((key) => {
      const element = createElement({
        tagName: 'div',
        className: `fighter-preview___${key}`,
        attributes: { title: fighter.name }
        // title: fighter[key]
      });
      element.innerHTML = `${key} - ${fighter.name}`.toUpperCase();
      return element;
    });
}

