import { fightersDetails, fighters } from './mockData';
import { HttpMethodEnum } from '../enum/http-method.enum';
import { FighterInterface } from '../interface/fighter.interface';
import { FighterDetailsInterface } from '../interface/fighter-details.interface';


const API_URL: string = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI: boolean = true;

async function callApi(endpoint: string,
                       method: 'GET' | 'POST' | 'PUT' | 'DELETE'): Promise<Array<FighterInterface> | FighterInterface> {
  const url = API_URL + endpoint;
  const options = {
    method
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
      .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
      .then((result) => JSON.parse(atob(result.content)))
      .catch((error) => {
        throw error;
      });
}

async function fakeCallApi(endpoint: string): Promise<Array<FighterInterface> | FighterDetailsInterface> {
  const response = endpoint === 'fighters.json' ? fighters : getFighterById(endpoint);

  return new Promise((resolve, reject) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
}

function getFighterById(endpoint: string): FighterDetailsInterface {
  const start = endpoint.lastIndexOf('/');
  const end = endpoint.lastIndexOf('.json');
  const id = endpoint.substring(start + 1, end);

  return <FighterDetailsInterface>fightersDetails.find((it) => it._id === id);
}

export { callApi };
