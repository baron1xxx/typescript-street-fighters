import { callApi } from '../helpers/apiHelper';
import { FighterDetailsInterface } from '../interface/fighter-details.interface';

class FighterService {
  async getFighters(): Promise<Array<FighterDetailsInterface>> {
    try {
      const endpoint = 'fighters.json';
      return <Array<FighterDetailsInterface>>await callApi(endpoint, 'GET');
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<FighterDetailsInterface> {
    const endpoint = `details/fighter/${id}.json`;
    return <FighterDetailsInterface>await callApi(endpoint, 'GET');
  }
}

export const fighterService = new FighterService();
