import { FighterInterface } from './fighter.interface';

export interface FighterDetailsInterface extends FighterInterface {
  health: number;
  attack: number;
  defense: number;
}
