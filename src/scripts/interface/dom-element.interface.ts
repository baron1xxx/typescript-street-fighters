export interface DomElementInterface {
  tagName: string;
  className?: string;
  attributes?: { [key: string]: string }; //TODO ще один інтерфейс.
}
